package id.ac.ui.cs.advprog.midterm.Controller;

import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import id.ac.ui.cs.advprog.midterm.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.Optional;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@SpringBootTest

public class UserControllerTest {

    @MockBean
    private UserRepository userRepository;

    private User users = new User();

    @BeforeEach
    void setUpUser() {
        users = new User();
        users.setId(18);
        users.setName("Inez");
        users.setEmail("inezzahra.nabila@gmail.com");
        when(userRepository.findById(users.getId()))
                .thenReturn(java.util.Optional.ofNullable(users));
    }


}


