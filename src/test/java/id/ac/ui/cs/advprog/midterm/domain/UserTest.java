package id.ac.ui.cs.advprog.midterm.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;


import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;


public class UserTest {

    @Autowired
    private User users;

    @BeforeEach
    public void setUp() {
        users = new User();
        users.setId(18);
        users.setName("Inez");
        users.setEmail("inezzahra.nabila@gmail.com");
    }

    @Test
    public void testIdSetterGetter(){
        users = new User();
        users.setId(18);
        assertEquals(18, users.getId());
    }

    @Test
    public void testNameSetterGetter(){
        users = new User();
        users.setName("Inez");
        assertEquals("Inez", users.getName());
        assertNotNull(users.getName());
    }

    @Test
    public void testEmailSetterGetter(){
        users = new User();
        users.setEmail("inezzahra.nabila@gmail.com");
        assertEquals("inezzahra.nabila@gmail.com", users.getEmail());
        assertNotNull(users.getEmail());
    }

    @Test
    public void testToString(){
        users = new User();
        users.setId(12345);
        users.setName("Inez");
        users.setEmail("inezzahra.nabila@gmail.com");
        assertEquals("User{id=12345, name='Inez', email='inezzahra.nabila@gmail.com'}", users.toString());
    }

}
